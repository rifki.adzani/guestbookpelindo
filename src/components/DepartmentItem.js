import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

export const DepartmentItem = ({data, onPress}) => {
  return (
    <TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          borderWidth: 1,
          borderColor: 'hsl(210, 5%, 70%)',
          justifyContent: 'space-between',
          paddingHorizontal: 15,
          paddingVertical: 10,
          backgroundColor: 'hsl(0, 0%, 98%)',
          borderRadius: 5,
        }}>
        <View style={{flex: 1}}>
          <Text style={{fontSize: 18, color: 'hsl(0, 0%, 5%)', fontWeight: '900'}}>
            {data.id}. {data.department}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};
