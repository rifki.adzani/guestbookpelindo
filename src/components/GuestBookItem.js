import React from 'react';
import {ScrollView, View, Text, TouchableOpacity, Image} from 'react-native';

export const GuestBookItem = ({data, onPress}) => {
  return (
    <TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          borderWidth: 1,
          borderColor: 'hsl(210, 5%, 70%)',
          justifyContent: 'space-between',
          paddingHorizontal: 15,
          paddingVertical: 10,
          backgroundColor: 'hsl(0, 0%, 98%)',
          borderRadius: 5,
        }}>
        <View style={{flex: 1}}>
          <Text style={{fontSize: 18, color: 'hsl(0, 0%, 5%)', fontWeight: '900'}}>
            {data.name} || {data.gender} || {data.department} || {data.notes}
          </Text>
          <Image
            source={data.avatar}
            style={{width: 200, height: 200}}
            resizeMode={'contain'}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};
