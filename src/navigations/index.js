import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Welcome from '../screens/Welcome.screen';
import Home from '../screens/Home.screen';
import About from '../screens/About.screen';
import Map from '../screens/Map.screen';
import EmployeeList from '../screens/EmployeeList.screen';
import DepartmentList from '../screens/DepartmentList.screen';
import Camera from '../screens/Camera.screen';
import GuestBook from '../screens/GuestBook.screen';
import GuestBookList from '../screens/GuestBookList.screen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainTab = (props) => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Welcome" component={Welcome} />
      <Tab.Screen name="About" component={About} />
    </Tab.Navigator>
  );
};

const RootNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Welcome" component={MainTab} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Map" component={Map} />
        <Stack.Screen name="EmployeeList" component={EmployeeList} />
        <Stack.Screen name="DepartmentList" component={DepartmentList} />
        <Stack.Screen name="Camera" component={Camera} />
        <Stack.Screen name="GuestBook" component={GuestBook} />
        <Stack.Screen name="GuestBookList" component={GuestBookList} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigator;
