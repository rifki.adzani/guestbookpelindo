import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import axios from 'axios';
import {GuestBookItem} from '../components/GuestBookItem';
import {FlatList} from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  topButton: {
    backgroundColor: 'hsl(0, 0%, 90%)',
    width: '45%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'hsl(0, 0%, 0%)',
    borderRadius: 5,
  },
  textLg: {
    color: '#000',
    fontSize: 18,
  },
  topButtonWrapper: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    marginVertical: 16,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  separator: {
    marginBottom: 10,
  },
  listContainer: {
    flex: 1,
    paddingHorizontal: 12,
  }
});

const API_URL = 'https://5f1cf1b039d95a0016953a78.mockapi.io/api/v1/guestbook';

export const GuestBookList = (props) => {
  const {navigation} = props;

  const [requestStatus, setRequestStatus] = useState('pending');
  const [guestbooks, setGuestBooks] = useState([]);

  useEffect(() => {
    async function getGuestBooks() {
      setRequestStatus('pending');

      const response = await axios
        .get(API_URL)
        .then((res) => res)
        .catch((err) => console.log('dari error' ,err));

      if (response.status == 200) { 
        setRequestStatus('succeeded');
        
        const mappedGuestBooks = response.data.map(guestbook => { return guestbook; });
         
        setGuestBooks(mappedGuestBooks);
      } 
      else 
      {
        setRequestStatus('failed');
      }
    }

    getGuestBooks();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.topButtonWrapper}>
       <Text>Data Guest Book</Text>
      </View>
      <View style={styles.listContainer}> 
        {requestStatus === 'pending' && <Text>Loading...</Text>}
        {requestStatus === 'succeeded' && (
          <FlatList
            data={guestbooks}
            renderItem={({item}) => <GuestBookItem data={item} />}
            keyExtractor={(item) => String(item.id)}
            ItemSeparatorComponent={() => <View style={styles.separator} />}
          />)}
        {requestStatus === 'failed' && <Text>Request Failed...</Text>}
      </View>
    </View>
  );
};

export default GuestBookList;
