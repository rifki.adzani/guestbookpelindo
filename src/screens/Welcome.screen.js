import * as React from 'react';
import { Button, View, Text } from 'react-native';

function Welcome({ navigation }) {
    return(
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>BUKU TAMU</Text>
        <Text>SELAMAT DATANG</Text>
        <Text>**************</Text>
        <Button
          title="E N T E R"
          onPress={() => navigation.navigate('Home')} />
      </View>
    );
}

export default Welcome;
