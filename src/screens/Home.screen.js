import * as React from 'react';
import { Button, View, Text } from 'react-native';

function Home({ navigation }) {
    return(
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>BUKU TAMU</Text>
        <Text>SELAMAT DATANG</Text>
        <Text>**************</Text>
        <Text> </Text>
        <Button
          title="PETA"
          onPress={() => navigation.navigate('Map')} />
        <Text> </Text>
        <Button
          title="DATA GUEST BOOK"
          onPress={() => navigation.navigate('GuestBookList')} />
        <Text> </Text>
        <Button
          title="DATA PEGAWAI"
          onPress={() => navigation.navigate('EmployeeList')} />
        <Text> </Text>
        <Button
          title="DATA DEPARTMENT"
          onPress={() => navigation.navigate('DepartmentList')} />
        <Text> </Text>
        <Button
          title="CAMERA"
          onPress={() => navigation.navigate('Camera')} />
        <Text> </Text>
        <Button
          title="GUEST BOOK"
          onPress={() => navigation.navigate('GuestBook')} />
      </View>
    );
}

export default Home;
