import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  Dimensions,
  StyleSheet,
  Button,
  ScrollView,
  Alert,
} from 'react-native';
import {Picker} from '@react-native-community/picker';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';

const {width, height} = Dimensions.get('window');

const DefaultInput = ({label, value, onChangeText, children}) => {
  return (
    <View style={[styles.row, styles.flexCenter]}>
      <View style={{width: width * 0.2}}>
        <Text style={{textAlign: 'left'}}>{label}</Text>
      </View>
      {!children && (
        <View
          style={{
            width: width * 0.6,
            borderBottomWidth: 1,
            borderBottomColor: '#ddd',
          }}>
          <TextInput value={value} onChangeText={(v) => onChangeText(v)} />
        </View>
      )}
      {children && children}
    </View>
  );
};

const GuestBook = (props) => {
  const [name, setName] = useState('');
  const [department, setDepartment] = useState('');
  const [avatar, setAvatar] = useState('');
  const [notes, setNotes] = useState('');
  const [requestStatus, setRequestStatus] = useState('initial');
  const API_URL = 'https://5f1cf1b039d95a0016953a78.mockapi.io/api/v1/guestbook';
  const [selectedValue, setSelectedValue] = useState("M");
  const handleSubmit = props => {
    const requestBody = {
      name: name,
      gender: selectedValue,
      department: department,
      notes: notes,
      avatar,
    };

    setRequestStatus('pending');

    axios
      .post(API_URL, requestBody)
      .then((_) => {
        Alert.alert('Save Succeeded');
        setRequestStatus('succeeded');
      })
      .catch((err) => console.log('dari error' ,err));
//       .catch((_) => {
//         Alert.alert('Save Failed');
//         setRequestStatus('failed');
//       });
  }

  const handlePickImage = () => {
    var options = {
      title: 'Select Picture',
      maxWidth:100,
      maxHeight:100,
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // const source = {uri: response.uri};

        // You can also display the image using data:
        const source = { uri: 'data:image/jpeg;base64,' + response.data };

        setAvatar(source);
      }
    });
  };

  return (
    <ScrollView style={{flex: 1, backgroundColor: '#FFF'}}>
      <View>
        <TouchableOpacity onPress={handlePickImage}>
          <Image
            source={avatar}
            style={{width, height: 200}}
            resizeMode={'contain'}
          />
        </TouchableOpacity>
      </View>
      <View>
        <DefaultInput label="Name" onChangeText={setName} />
        <DefaultInput label="Department" onChangeText={setDepartment} />
        <DefaultInput label="Gender">
          <Picker
            style={{width: '60%', height: 50}}
            selectedValue={selectedValue}
            onValueChange={(itemValue) => setSelectedValue(itemValue)}>
            <Picker.Item label="Male" value="M" />
            <Picker.Item label="Female" value="F" />
          </Picker>
        </DefaultInput>
        <DefaultInput label="Notes" onChangeText={setNotes} />
        <View style={{alignItems: 'center', marginTop: 30}}>
          <Button
            style={{width: 100}}
            title={requestStatus === 'pending' ? 'Loading...' : 'SAVE'}
            onPress={handleSubmit}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default GuestBook;

const styles = StyleSheet.create({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  flexCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
  image: {
    width,
    height: 200,
  },
});
