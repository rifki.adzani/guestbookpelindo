import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import axios from 'axios';
import {EmployeeItem} from '../components/EmployeeItem';
import {FlatList} from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  topButton: {
    backgroundColor: 'hsl(0, 0%, 90%)',
    width: '45%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'hsl(0, 0%, 0%)',
    borderRadius: 5,
  },
  textLg: {
    color: '#000',
    fontSize: 18,
  },
  topButtonWrapper: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    marginVertical: 16,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  separator: {
    marginBottom: 10,
  },
  listContainer: {
    flex: 1,
    paddingHorizontal: 12,
  }
});

const API_URL = 'https://run.mocky.io/v3/1187025a-d4d1-41ef-8732-10ef7fa793f6';

export const EmployeeList = (props) => {
  const {navigation} = props;

  const [requestStatus, setRequestStatus] = useState('pending');
  const [employees, setEmployees] = useState([]);

  useEffect(() => {
    async function getEmployees() {
      setRequestStatus('pending');

      const response = await axios
        .get(API_URL)
        .then((res) => res)
        .catch((err) => err);

      if (response.status == 200) { 
        setRequestStatus('succeeded');
        
        const mappedEmployees = (response.data).data.map(employee => { return employee; });
        
        setEmployees(mappedEmployees);
      } 
      else 
      {
        setRequestStatus('failed');
      }
    }

    getEmployees();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.topButtonWrapper}>
       <Text>Data Pegawai</Text>
      </View>
      <View style={styles.listContainer}> 
        {requestStatus === 'pending' && <Text>Loading...</Text>}
        {requestStatus === 'succeeded' && (
          <FlatList
            data={employees}
            renderItem={({item}) => <EmployeeItem data={item} />}
            keyExtractor={(item) => String(item.id)}
            ItemSeparatorComponent={() => <View style={styles.separator} />}
          />)}
        {requestStatus === 'failed' && <Text>Request Failed...</Text>}
      </View>
    </View>
  );
};

export default EmployeeList;
