import MapView from 'react-native-maps';
import { View } from 'react-native';
import * as React from 'react';

function Map() {
  return(
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <MapView
        style={{ 
          width:350,
          height:350}}
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421}}>

      <MapView.Marker
        coordinate={{
            latitude: 37.78825,
            longitude: -122.4324}}
        title="Lokasi"
        description="Hello" />
      </MapView>
    </View>
  )};

export default Map;
