import React from 'react';
import RootNavigator from './src/navigations';

export default function App() {
  return <RootNavigator />;
}
